package clientManager

import (
	"horo.x3ro.dev/m/server/client"
)

type ClientManager struct {
	clients []*client.Client
}

func NewClientManager() ClientManager {
	return ClientManager{
		clients: make([]*client.Client, 0),
	}
}

func (cm *ClientManager) AddClient(client *client.Client) {
	cm.clients = append(cm.clients, client)
}

func (cm *ClientManager) RemoveClient(client *client.Client) {
	for i, c := range cm.clients {
		if c == client {
			cm.clients = append(cm.clients[:i], cm.clients[i+1:]...)
			break
		}
	}
}

func (cm *ClientManager) Clients() []*client.Client {
	return cm.clients
}
