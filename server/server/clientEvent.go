package server

import "horo.x3ro.dev/m/server/client"

type clientEventEnum int

const (
	CLIENT_CONNECT clientEventEnum = iota
	CLIENT_DISCONNECT
)

type clientEvent struct {
	evtType clientEventEnum
	client  *client.Client
}
