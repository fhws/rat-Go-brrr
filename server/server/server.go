package server

import (
	"fmt"
	"log"
	"net"
	"os"
	"strings"

	"horo.x3ro.dev/m/lib/protocol"
	"horo.x3ro.dev/m/server/client"
	"horo.x3ro.dev/m/server/server/clientManager"
)

type Server struct {
	listen          string
	listener        net.Listener
	connectedClient *client.Client
	clients         clientManager.ClientManager
}

func NewServer(listen string) Server {
	return Server{
		listen:  listen,
		clients: clientManager.NewClientManager(),
	}
}

func (server *Server) Close() {
	if server.listener != nil {
		server.listener.Close()
	}
	// TODO: Close connections, etc.
}

func (sever *Server) Start() {
	sever.startListen()

	clientEventChan := make(chan clientEvent)
	go sever.manageClients(clientEventChan)

	sever.handleConnections(clientEventChan)
}

func (server *Server) startListen() {
	listener, err := net.Listen("tcp", server.listen)
	if err != nil {
		log.Println("ERROR:", err)
		fmt.Fprintln(os.Stderr, "Failed listening on", server.listen)
		os.Exit(1)
	}
	server.listener = listener

	log.Println("Server listening on", server.listen)
}

func (server *Server) manageClients(evtChan <-chan clientEvent) {
	for evt := range evtChan {
		name := evt.client.Name
		addr := evt.client.Conn.RemoteAddr().String()

		switch evt.evtType {

		case CLIENT_CONNECT:
			log.Printf("--- Client connected: %s (%s)", name, addr)
			server.clients.AddClient(evt.client)

		case CLIENT_DISCONNECT:
			log.Printf("--- Client disconnected: %s (%s)", name, addr)
			server.clients.RemoveClient(evt.client)

			// HACK: This does not belong here
			if evt.client == server.connectedClient {
				server.connectedClient = nil
			}
		}
	}
}
func (sever *Server) handleConnections(clientEventChan chan clientEvent) {
	for {
		conn, err := sever.listener.Accept()
		if err != nil {
			fmt.Fprintln(os.Stderr, "Could not accept new connection:", err)
			os.Exit(1)
		}

		go sever.handleConnection(conn, clientEventChan)
	}
}

func (server *Server) handleConnection(conn net.Conn, clientEventChan chan<- clientEvent) {
	defer func() {
		conn.Close()
		log.Printf("--- Connection closed: %s", conn.RemoteAddr().String())
	}()

	log.Printf("--- New connection: %s", conn.RemoteAddr().String())

	reader := protocol.NewMsgReader(conn)

	client := &client.Client{
		Name: "",
		Conn: conn,
	}

	// Throw away everything the client sends us unless the server is connectec
	// to it
	for {
		msg, err := reader.Read()
		if err != nil {
			return
		}

		if msg.Type() != protocol.MSG_NAME {
			continue
		}

		client.Name = string(msg.Data())

		clientEventChan <- clientEvent{
			evtType: CLIENT_CONNECT,
			client:  client,
		}

		break
	}

	var multiPartFileWriter *protocol.MultipartWriter

	for {
		msg, err := reader.Read()
		if err != nil {
			break
		}

		if client == server.connectedClient {
			switch msg.Type() {

			case protocol.MSG_STDOUT:
				fmt.Fprint(os.Stdout, string(msg.Data()))

			case protocol.MSG_STDERR:
				fmt.Fprint(os.Stderr, string(msg.Data()))

			case protocol.MSG_DETACH:
				fmt.Fprint(os.Stdout, "Client detached (press enter to return to server console)")
				server.connectedClient = nil

			case protocol.MSG_FILE_MULTIPART_START:
				if multiPartFileWriter != nil {
					fmt.Fprintln(os.Stderr, "ERROR: Protocol Error: Received new file upload message while a file upload is already in progress")
					return
				}

				fname := string(msg.Data())
				fname = strings.TrimSpace(fname)

				file, err := os.OpenFile(fname, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
				if err != nil {
					fmt.Fprintln(os.Stderr, "ERROR: Failed to create file for upload:", err)
					return
				}

				multiPartFileWriter = protocol.NewMultipartWriter(file, protocol.MSG_FILE_MULTIPART)
				defer multiPartFileWriter.Close()

			case protocol.MSG_FILE_MULTIPART:
				if multiPartFileWriter == nil {
					fmt.Fprintln(os.Stderr, "ERROR: Protocol Error: Received file upload message while no file upload is already in progress")
					return
				}
				_, err := multiPartFileWriter.WritePart(*msg)
				if err != nil {
					fmt.Fprintln(os.Stderr, "ERROR: Error while writing multipart file:", err)
					return
				}

			case protocol.MSG_FILE_MULTIPART_END:
				if multiPartFileWriter == nil {
					fmt.Fprintln(os.Stderr, "ERROR: Protocol Error: Received file upload end message while no file upload is already in progress")
					return
				}
				err := multiPartFileWriter.Close()
				if err != nil {
					fmt.Fprintln(os.Stderr, "ERROR: Error closing file:", err)
					return
				}
				multiPartFileWriter = nil
			}
		}
	}

	clientEventChan <- clientEvent{
		evtType: CLIENT_DISCONNECT,
		client:  client,
	}
}

func (server *Server) Clients() []*client.Client {
	return server.clients.Clients()
}

func (server *Server) ConnectClient(client *client.Client) {
	server.connectedClient = client
}

func (server *Server) ConnectedClient() *client.Client {
	return server.connectedClient
}
