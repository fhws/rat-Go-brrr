package protocol

import (
	"bufio"
	"io"
	"strconv"
	"strings"
)

type MsgReader struct {
	reader *bufio.Reader
}

func NewMsgReader(reader io.Reader) *MsgReader {
	return &MsgReader{
		reader: bufio.NewReader(reader),
	}
}

func (r *MsgReader) Read() (*Message, error) {
	msgType, err := r.readHeader()
	if err != nil {
		return nil, err
	}

	data, err := r.readData()
	if err != nil {
		return nil, err
	}

	msg := NewMessage(msgType, data)
	return &msg, nil
}

func (r *MsgReader) readHeader() (MsgType, error) {
	msgType, err := r.reader.ReadString(' ')
	if err != nil {
		return "", err
	}
	msgType = strings.TrimSpace(msgType)

	return ParseMsgType(msgType)
}

func (r *MsgReader) readData() ([]byte, error) {
	lenStr, err := r.reader.ReadString(' ')
	if err != nil {
		return nil, err
	}
	lenStr = strings.TrimSpace(lenStr)

	len, err := strconv.Atoi(lenStr)
	if err != nil {
		return nil, err
	}

	buf := make([]byte, len)
	_, err = io.ReadFull(r.reader, buf)
	if err != nil {
		return nil, err
	}

	return buf, nil
}
