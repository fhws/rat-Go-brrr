package protocol

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"errors"
	"io"
	"io/ioutil"
)

type MultipartWriter struct {
	writer  io.WriteCloser
	msgType MsgType
	offset  int
}

func NewMultipartWriter(pipe io.WriteCloser, msgType MsgType) *MultipartWriter {
	return &MultipartWriter{
		writer:  pipe,
		msgType: msgType,
		offset:  0,
	}
}

func (m *MultipartWriter) WritePart(msg Message) (int, error) {
	if msg.Type() != m.msgType {
		return 0, errors.New("multipart message type does not match")
	}

	r := bufio.NewReader(bytes.NewReader(msg.Data()))

	totalLen, err := readUint64(r)
	if err != nil {
		return 0, err
	}

	offset, err := readUint64(r)
	if err != nil {
		return 0, err
	}
	if offset != uint64(m.offset) {
		return 0, errors.New("offset of multipart message does not match current offset")
	}

	dataLen, err := readUint64(r)
	if err != nil {
		return 0, err
	}
	if m.offset+int(dataLen) > int(totalLen) {
		return 0, errors.New("data length of multipart message exceeds total length")
	}

	data, err := ioutil.ReadAll(r)
	if err != nil {
		return 0, err
	}
	if len(data) != int(dataLen) {
		return 0, errors.New("length of data section does not match expected length")
	}

	i, err := m.writer.Write(data)
	m.offset += i

	return i, err
}

func readUint64(r io.Reader) (uint64, error) {
	bs := make([]byte, 8)
	_, err := r.Read(bs)
	if err != nil {
		return 0, err
	}
	return binary.LittleEndian.Uint64(bs), nil
}

func (m *MultipartWriter) Close() error {
	return m.writer.Close()
}
