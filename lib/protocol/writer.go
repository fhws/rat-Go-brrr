package protocol

type Writer struct {
	msgType   MsgType
	msgWriter *MsgWriter
}

func NewWriter(msgType MsgType, msgWriter *MsgWriter) *Writer {
	return &Writer{
		msgType:   msgType,
		msgWriter: msgWriter,
	}
}

func (w Writer) Write(data []byte) (n int, err error) {
	_, nData, err := w.msgWriter.Write(NewMessage(w.msgType, data))
	return nData, err
}
