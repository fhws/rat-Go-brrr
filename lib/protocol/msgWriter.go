package protocol

import (
	"encoding/binary"
	"fmt"
	"io"

	"horo.x3ro.dev/m/lib/filesize"
)

type MsgWriter struct {
	writer io.Writer
}

func NewMsgWriter(writer io.Writer) *MsgWriter {
	return &MsgWriter{
		writer: writer,
	}
}

func (w *MsgWriter) Write(msg Message) (nHeader int, nData int, err error) {
	nHeader, err = fmt.Fprintf(w.writer, "%v %d ", msg.Type(), msg.Len())
	if err != nil {
		return nHeader, 0, err
	}
	nData, err = w.writer.Write(msg.Data())
	return nHeader, nData, err
}

func (w *MsgWriter) WriteMsg(msgType MsgType, data []byte) (nHeader int, nData int, err error) {
	return w.Write(NewMessage(msgType, data))
}

func (w *MsgWriter) WriteStringMsg(msgType MsgType, data string) (nHeader int, nData int, err error) {
	return w.Write(NewMessage(msgType, []byte(data)))
}

func (w *MsgWriter) WriteEmptyMsg(msgType MsgType) (nHeader int, nData int, err error) {
	return w.Write(NewMessage(msgType, make([]byte, 0)))
}

func (w *MsgWriter) WriteMultipartMsg(msgType MsgType, data io.ReadSeeker) (nHeader int, nData int, err error) {
	return w.WriteMultipartMsgStatus(msgType, data, func(_, _ int) {})
}

func (w *MsgWriter) WriteMultipartMsgStatus(msgType MsgType, data io.ReadSeeker, update func(bytesWritten, bytesTotal int)) (nHeader int, nData int, err error) {
	bufLen := 5 * filesize.MiB

	streamLen, err := getStreamLen(data)
	if err != nil {
		return 0, 0, err
	}

	var readErr error
	nHeaderSum := 0
	offset := 0

	for readErr != io.EOF {
		// Read from stream
		buf := make([]byte, bufLen)
		readLen, err := data.Read(buf)
		if err != nil && err != io.EOF {
			return nHeaderSum, offset, err
		}
		readErr = err

		// TODO: `break` if `readLen` is 0?

		d := make([]byte, 0)
		d = append(d, int64ToBin(streamLen)...)
		d = append(d, intToBin(offset)...)
		d = append(d, int64ToBin(int64(readLen))...)

		innerHeaderLen := len(d)

		d = append(d, buf[0:readLen]...)

		nHeader, _, err := w.Write(NewMessage(msgType, d))
		nHeaderSum += nHeader + innerHeaderLen
		offset += readLen
		if err != nil {
			return nHeaderSum, offset, err
		}

		update(offset, int(streamLen))
	}

	return nHeaderSum, offset, nil
}

func intToBin(i int) []byte {
	return int64ToBin(int64(i))
}

func int64ToBin(i int64) []byte {
	bs := make([]byte, 8)
	binary.LittleEndian.PutUint64(bs, uint64(i))
	return bs
}

func (w *MsgWriter) Writer(msgType MsgType) *Writer {
	return NewWriter(msgType, w)
}

func getStreamLen(stream io.ReadSeeker) (int64, error) {
	offsetCurrent, err := stream.Seek(0, io.SeekCurrent)
	if err != nil {
		return 0, err
	}

	offsetEnd, err := stream.Seek(0, io.SeekEnd)
	if err != nil {
		return 0, err
	}

	_, err = stream.Seek(offsetCurrent, io.SeekStart)
	if err != nil {
		return 0, err
	}

	len := offsetEnd - offsetCurrent

	return len, nil
}
