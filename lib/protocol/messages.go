package protocol

import (
	"errors"
)

var (
	ErrUnknownMessageType = errors.New("unknown message type")
)

type MsgType string

const (
	MSG_ATTACH               MsgType = "ATTACH"
	MSG_DETACH               MsgType = "DETACH"
	MSG_STDOUT               MsgType = "STDOUT"
	MSG_STDERR               MsgType = "STDERR"
	MSG_STDIN                MsgType = "STDIN"
	MSG_NAME                 MsgType = "NAME"
	MSG_FILE_MULTIPART_START MsgType = "FILE_MULTIPART_START"
	MSG_FILE_MULTIPART       MsgType = "FILE_MULTIPART"
	MSG_FILE_MULTIPART_END   MsgType = "FILE_MULTIPART_END"
)

func ParseMsgType(msgType string) (MsgType, error) {
	mt := MsgType(msgType)
	switch mt {
	case
		MSG_ATTACH,
		MSG_DETACH,
		MSG_STDOUT,
		MSG_STDERR,
		MSG_STDIN,
		MSG_NAME,
		MSG_FILE_MULTIPART_START,
		MSG_FILE_MULTIPART,
		MSG_FILE_MULTIPART_END:
		return mt, nil
	default:
		return "", ErrUnknownMessageType
	}
}

type Message struct {
	msgType MsgType
	data    []byte
}

func NewMessage(msgType MsgType, data []byte) Message {
	return Message{
		msgType: msgType,
		data:    data,
	}
}

func NewEmptyMessage(msgType MsgType) Message {
	return NewStringMessage(msgType, "")
}

func NewStringMessage(msgType MsgType, data string) Message {
	return NewMessage(msgType, []byte(data))
}

func NewAttachMessage() Message {
	return NewEmptyMessage(MSG_ATTACH)
}

func NewDetachMessage() Message {
	return NewEmptyMessage(MSG_DETACH)
}

func (m Message) Type() MsgType {
	return m.msgType
}

func (m Message) Len() int {
	return len(m.data)
}

func (m Message) Data() []byte {
	return m.data
}
