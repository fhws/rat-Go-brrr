package download

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
)

type RequestError struct {
	StatusCode int
	Err        error
}

func (r *RequestError) Error() string {
	return fmt.Sprintf("status %d: err %v", r.StatusCode, r.Err)
}

func DownloadFile(targetFilePath string, url string) (filePath string, err error) {
	resp, err := http.Get(url)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", &RequestError{
			StatusCode: resp.StatusCode,
			Err:        errors.New("could not download - answer was not 200 (OK)"),
		}
	}

	if targetFilePath == "" {
		targetFilePath = getNameFromResp(resp)
	} else {
		err := os.MkdirAll(filepath.Dir(targetFilePath), os.ModePerm)
		if err != nil {
			return "", err
		}
	}

	out, err := os.Create(targetFilePath)
	if err != nil {
		targetFilePath = "download.bin"
		out, err = os.Create(targetFilePath)

		if err != nil {
			// error is not filepath depended
			return targetFilePath, err
		}
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)
	return targetFilePath, nil
}

func getNameFromResp(resp *http.Response) string {
	contentDisp := resp.Header.Get("Content-Disposition")
	splited := strings.Split(contentDisp, ";")

	for _, item := range splited {
		keyValue := strings.SplitN(strings.Trim(item, " "), "=", 2)

		if len(keyValue) == 2 && strings.ToLower(keyValue[0]) == "filename" {
			return keyValue[1]
		}
	}

	return path.Base(resp.Request.URL.Path)
}
