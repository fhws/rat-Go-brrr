package modules

import (
	"strings"

	"horo.x3ro.dev/m/config"
)

func GetExe(mod string) string {
	return strings.ReplaceAll(config.MODULES_NAME_TPL, "{MODULE_NAME}", mod)
}
func GetURL(mod string) string {
	return strings.ReplaceAll(config.MODULES_URL_TPL, "{MODULE_NAME}", mod)
}
