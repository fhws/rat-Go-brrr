package args

import (
	"errors"
	"strings"
)

func Fields(s string) (fields []string, err error) {
	sb := &strings.Builder{}
	fields = []string{}

	quoted := false
	quote := ' '
	escaped := false

	for _, r := range s {
		if escaped {
			escaped = false

			if quoted && r != quote {
				// Nothing to escape
				sb.WriteRune('\\')
			}

			sb.WriteRune(r)
			continue
		}

		if r == '\\' && (!quoted || quote != '\'') {
			escaped = true
			continue
		}

		if quoted {
			if r == quote {
				quoted = false
				continue
			}
			sb.WriteRune(r)
			continue
		}

		if r == '\'' || r == '"' {
			quote = r
			quoted = true
			continue
		}

		if r == ' ' {
			fields = append(fields, sb.String())
			sb.Reset()
			continue
		}

		sb.WriteRune(r)
	}

	if sb.Len() > 0 {
		fields = append(fields, sb.String())
	}

	if quoted {
		return nil, errors.New("missing closing quote: `" + string(quote) + "`")
	}
	if escaped {
		return nil, errors.New("unexpected escape symbol at end of string")
	}

	return fields, nil
}
