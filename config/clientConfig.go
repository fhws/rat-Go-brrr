package config

import "time"

const (
	CONNECT           = "localhost:8080"
	MODULES_PATH      = "modules/"
	RECONNECT_TIMEOUT = 1 * time.Second
)
