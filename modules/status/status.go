package main

import (
	"fmt"
	"horo.x3ro.dev/m/lib/modules"
	"log"
	"os/exec"
	"strings"
)

func main() {
	fmt.Println("Hostname:")
	fmt.Println("----------")
	fmt.Println(callModule("hostname"))
	fmt.Println()
	fmt.Println("CPU Info:")
	fmt.Println("----------")
	fmt.Println(callModule("cpuInfo"))
	fmt.Println()
	fmt.Println("Host Info:")
	fmt.Println("-----------")
	fmt.Println(callModule("hostInfo"))
	fmt.Println()
	fmt.Println("Memory Info:")
	fmt.Println("-------------")
	fmt.Println(callModule("memory"))
	fmt.Println()
	fmt.Println("Processes Info:")
	fmt.Println("----------------")
	fmt.Println(callModule("ps"))
	fmt.Println()
	fmt.Println("Uptime Info:")
	fmt.Println("-------------")
	fmt.Println(callModule("uptime"))
	fmt.Println()
	fmt.Println("IP Info:")
	fmt.Println("---------")
	fmt.Println(callModule("ipAddr"))
}

func callModule(processName string) (output string) {
	exeOfModule := modules.GetExe(processName)
	out, err := exec.Command(exeOfModule).Output()
	if err != nil {
		log.Println("ERROR:", processName, "module was not found in `"+exeOfModule+"`")
		return ""
	}
	return strings.Trim(string(out), " \n")
}
