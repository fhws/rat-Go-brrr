package main

import (
	"fmt"
	"github.com/pbnjay/memory"
)

func main() {
	fmt.Printf("RAM: %.2fGB\n", float64(memory.TotalMemory())/1024/1024/1024)
}
