package main

import (
	"fmt"
	"image"
	"image/png"
	"os"
	"path/filepath"
	"time"

	"github.com/kbinani/screenshot"
)

const (
	dateFmt           string = "2006-01-02 15-04-05 -0700 UTC"
	screenshotNameFmt string = "screenshot_%s.png"
)

func main() {
	img, _ := CaptureAllScreens()

	fileName := "screenshot_" + getTimeStamp() + ".png"

	file, _ := os.Create(fileName)
	defer file.Close()

	png.Encode(file, img)

	screenshotPath, _ := filepath.Abs(file.Name())
	fmt.Println(screenshotPath)
}

func getTimeStamp() string {
	time := time.Now().UTC()
	return time.Format(dateFmt)
}

func CaptureAllScreens() (*image.RGBA, error) {
	allScreenRect := GetAllDisplayBounds()
	return screenshot.CaptureRect(allScreenRect)
}

func GetAllDisplayBounds() image.Rectangle {
	screenCount := screenshot.NumActiveDisplays()

	maxX := 0
	maxY := 0
	minX := 0
	minY := 0

	for i := 0; i < screenCount; i++ {
		currentMaxX := screenshot.GetDisplayBounds(i).Max.X
		if maxX < currentMaxX {
			maxX = currentMaxX
		}

		currentMaxY := screenshot.GetDisplayBounds(i).Max.Y
		if maxY < currentMaxY {
			maxY = currentMaxY
		}

		currentMinX := screenshot.GetDisplayBounds(i).Min.X
		if minX > currentMinX {
			minX = currentMinX
		}

		CurrentMinY := screenshot.GetDisplayBounds(i).Min.Y
		if minY > CurrentMinY {
			minY = CurrentMinY
		}
	}

	return image.Rect(minX, minY, maxX, maxY)
}
