package main

import (
	"fmt"
	"github.com/shirou/gopsutil/host"
)

func main() {
	hostInfo, _ := host.Info()

	upTimeInMin := float64(hostInfo.Uptime) / 60
	upTimeInHours := int(upTimeInMin) / 60
	upTimeInDays := int(upTimeInHours) / 24

	fmt.Println("UpTime:")
	fmt.Printf("    Days: %d\n", upTimeInDays)
	fmt.Printf("    Hours: %d\n", upTimeInHours-upTimeInDays*24)
	fmt.Printf("    Minutes: %.2f\n", upTimeInMin-float64(upTimeInHours*60))
}
