package main

import (
	"fmt"
	"github.com/shirou/gopsutil/host"
)

func main() {
	hostInfo, _ := host.Info()
	fmt.Println("Processes:", hostInfo.Procs)
}
