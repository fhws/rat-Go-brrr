package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
)

func main() {
	ip, mask := getIPAddrAndMaskOutbound()
	fmt.Println("Local preferred IP:", ip, "/", mask)

	IPInfos := getPublicIP()

	fmt.Println("Public IP:", IPInfos.Query)
	fmt.Println("   Isp:", IPInfos.Isp)
	fmt.Println("   Country:", IPInfos.Country)
	fmt.Println("   Region:", IPInfos.RegionName)
	fmt.Println("   City:", IPInfos.City)
	fmt.Println("   Zip:", IPInfos.Zip)
}

// Get preferred outbound ip of this machine (without connectin to anything!)
// Udp should not req. admin access!
// This method should always work since 198.51.100.0 is never used and for
// `Assigned as TEST-NET-2, documentation and examples`
// Source: `https://en.wikipedia.org/wiki/Reserved_IP_addresses`
func getIPAddrAndMaskOutbound() (net.IP, int) {
	conn, err := net.Dial("udp", "198.51.100.0:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)
	ip := localAddr.IP
	mask, _ := ip.DefaultMask().Size()

	return ip, mask
}

type PubIPInfo struct {
	Status      string  `json:"status"`
	Country     string  `json:"country"`
	CountryCode string  `json:"countryCode"`
	Region      string  `json:"region"`
	RegionName  string  `json:"regionName"`
	City        string  `json:"city"`
	Zip         string  `json:"zip"`
	Lat         float64 `json:"lat"`
	Lon         float64 `json:"lon"`
	Timezone    string  `json:"timezone"`
	Isp         string  `json:"isp"`
	Org         string  `json:"org"`
	As          string  `json:"as"`
	Query       string  `json:"query"`
}

func getPublicIP() PubIPInfo {
	req, err := http.Get("http://ip-api.com/json/")
	if err != nil {
		log.Println(err.Error())
		os.Exit(1)
	}
	defer req.Body.Close()

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Println(err.Error())
	}

	var result PubIPInfo
	_ = json.Unmarshal(body, &result)

	return result
}
