package main

import (
	"fmt"
	"github.com/shirou/gopsutil/host"
)

func main() {
	hostInfo, _ := host.Info()

	fmt.Println("OS:", hostInfo.OS)
	fmt.Println("KernelArch:", hostInfo.KernelArch)
	fmt.Println("KernelVersion:", hostInfo.KernelVersion)
	fmt.Println("Platform:", hostInfo.Platform)
	fmt.Println("PlatformFamily:", hostInfo.PlatformFamily)
	fmt.Println("PlatformVersion:", hostInfo.PlatformVersion)
	fmt.Println("VirtualizationRole:", hostInfo.VirtualizationRole)
	fmt.Println("VirtualizationSystem:", hostInfo.VirtualizationSystem)
}
