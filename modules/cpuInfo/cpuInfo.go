package main

import (
	"fmt"
	"github.com/shirou/gopsutil/cpu"
)

func main() {
	cpuInfoArray, _ := cpu.Info()
	cpuInfo := cpuInfoArray[0]

	fmt.Println("CPU VendorID:", cpuInfo.VendorID)
	fmt.Println("CPU ModelName:", cpuInfo.ModelName)
	fmt.Println("CPU Family:", cpuInfo.Family)
	fmt.Println("CPU Stepping:", cpuInfo.Stepping)
	fmt.Println("CPU Cores:", cpuInfo.Cores)
	fmt.Println("CPU Mhz:", cpuInfo.Mhz)
}
