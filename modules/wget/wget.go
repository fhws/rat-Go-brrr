package main

import (
	"flag"
	"fmt"
	"horo.x3ro.dev/m/lib/download"
	"log"
	"os"
)

func main() {
	var url string
	var filepath string
	flag.StringVar(&url, "url", "", "url to the file to download.")
	flag.StringVar(&filepath, "filepath", "", "filepath to "+
		"save the download in. If no filepath is given the filename from the "+
		"file to download is used")

	flag.Parse()

	targetFilePath, err := download.DownloadFile(filepath, url)

	if err != nil {
		re, ok := err.(*download.RequestError)
		if ok {
			_, _ = fmt.Fprintln(os.Stderr, re)
			os.Exit(1)
		} else {
			log.Panicln(err)
		}
	}

	fmt.Println("Done - Downloaded: " + url + " to " + targetFilePath)
}
