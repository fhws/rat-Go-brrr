module horo.x3ro.dev/m

go 1.16

require (
	github.com/StackExchange/wmi v0.0.0-20210224194228-fe8f1750fd46 // indirect
	github.com/go-ole/go-ole v1.2.5 // indirect
	github.com/kbinani/screenshot v0.0.0-20210326165202-b96eb3309bb0
	github.com/pbnjay/memory v0.0.0-20201129165224-b12e5d931931
	github.com/shirou/gopsutil v3.21.6+incompatible // indirect
	github.com/tklauser/go-sysconf v0.3.6 // indirect
)
